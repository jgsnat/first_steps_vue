import Vue from 'vue'
import Router from 'vue-router'
import App from '../App'
import CepChecker from '../CepChecker'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'App',
      component: App
    },
    {
      path: '/cep',
      name: 'cep',
      component: CepChecker
    }
  ]
})
