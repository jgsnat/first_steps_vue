// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import VueEvents from './plugins/events'
import store from './store/index'

Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.use(VueEvents)
Vue.use(VueResource)

new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  // template: '<App/>'
  template: `
    <div id="app">
      <transition name="fade" mode="out-in">
        <router-view class="view"></router-view>
      </transition>
    </div>
  `
})
