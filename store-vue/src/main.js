import Vue from 'vue'
import loja from './loja/raiz'
import App from './App.vue'

new Vue({
  store: loja,
  el: '#app',
  render: h => h(App)
})
